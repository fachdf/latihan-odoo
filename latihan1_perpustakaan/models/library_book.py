# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

class Book(models.Model):
    _name = 'library.book'

    name = fields.Char(string='Name')
    code = fields.Char(string='Code')
    date_of_issue = fields.Date(string='Date of Issue')
    author = fields.Many2many(
       comodel_name='res.partner', 
       string='Author'
    )
    image_1920 = fields.Binary(string='Image')
   










# from odoo import models, fields, api


# class latihan(models.Model):
#     _name = 'latihan.latihan'
#     _description = 'latihan.latihan'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100

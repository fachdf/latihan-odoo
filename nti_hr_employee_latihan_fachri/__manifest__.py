# -*- coding: utf-8 -*-
{
    'name': "nti_Employee",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Modul adalah module mengenai employee dari suatu perusahaan, dibuat untuk eksplorasi odoo.
    """,
    'author': "Fachri Dhia Fauzan",
    'website': "",
    'category': 'Uncategorized',
    'version': '0.1',

    # Tambahkan depend ke module 'base' dan 'hr'
    'depends': ['base', 'hr'],

    # Daftarkan file view & security
    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee_views.xml',
        'views/hr_certificate_views.xml',
        ],
    'demo': [],
    'application': True
}

# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError


class HRCertificate(models.Model):
    _name = 'hr.certificate'

    name = fields.Char(string='Name')
    type = fields.Selection(
        [
            ("seminar","Seminar"),
            ("training","Training"),
            ("competition","Competition"),
            ("appreciation","Appreciation"),
        ], 
        
        string='Type')

    attachment = fields.Many2many(
        comodel_name='ir.attachment', 
        string='Attachment'
    )
    
    issued_date = fields.Date(string='Issued Date')

    employee_id = fields.Many2one('hr.employee', string='Employee',)


    # Tambahkan field berikut :
    # Name - String
    # Type - Selection dengan element (Seminar,Training,Competition,Appreciation)
    # Attachment - M2M (Many-to-Many) terhadap model 'ir.attachment'
    # Issued Date - Date
    # Employee - Many-to-One terhadap hr.employee